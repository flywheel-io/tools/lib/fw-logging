"""Tests for the fw_logging module."""

import datetime
import inspect
import json
import logging
import logging.config
import os
import re

import pytest
from devtools.ansi import sformat, strip_ansi

import fw_logging

os.environ["FW_LOG_CALLFMT"] = callfmt = "^tests"
logging.getLogger("asyncio").setLevel(logging.WARNING)


def test_default_log_config_values():
    config = fw_logging.get_log_config()

    assert config["version"] == 1
    assert config["disable_existing_loggers"] is False
    assert config["root"]["level"] == "INFO"
    assert config["root"]["handlers"] == ["stdout"]
    assert config["handlers"]["stdout"]["formatter"] == "text"
    assert config["formatters"]["text"]["callfmt"] == "^tests"


def test_custom_log_config_values():
    config = fw_logging.get_log_config(
        level="DEBUG",
        handler="file",
        filename="filename",
        formatter="json",
        json_tag="tag",
        loggers={"verbose.lib": "CRITICAL"},
        filters={"filter_name": {"msg": "ignore pattern"}},
    )

    assert config["root"]["level"] == "DEBUG"
    assert config["root"]["handlers"] == ["file"]
    assert config["loggers"]["verbose.lib"]["level"] == "CRITICAL"
    assert config["loggers"]["verbose.lib"]["handlers"] == ["file"]
    assert config["filters"]["filter_name"]["msg"] == "ignore pattern"
    assert config["handlers"]["file"]["filename"] == "filename"
    assert config["handlers"]["file"]["filters"] == ["filter_name"]
    assert config["handlers"]["file"]["formatter"] == "json"
    assert config["formatters"]["json"]["tag"] == "tag"

    logging.config.dictConfig(config)


TEST_DATETIME = datetime.datetime(1999, 12, 31, 23, 59, 59, 12345)
TIMESTAMP = "1999-12-31T23:59:59.012"


@pytest.fixture(scope="function", autouse=True)
def mock_time(mocker):
    timestamp = TEST_DATETIME.timestamp()
    mocker.patch("time.time", return_value=timestamp)
    mocker.patch("time.time_ns", return_value=int(timestamp * 10**9))  # 3.13


@pytest.fixture(scope="function")
def get_log(capsys):
    """Return lambda for getting the log printed to stdout."""
    return lambda: capsys.readouterr().out.strip()


def colorize(color: str, msg: str, bold=False) -> str:
    """Return colorized string from color name."""
    args = ["bold"] if bold else []
    args.append(color)
    return sformat(msg, *args)


def caller(*, logger="root", module="tests.test_fw_logging", offset=-1) -> str:
    """Return formatted logrecord caller info assuming the previous line no."""
    lineno = inspect.currentframe().f_back.f_lineno + offset  # type: ignore
    return f"{module}:{lineno}" if re.match(callfmt, module) else logger


@pytest.mark.parametrize(
    "levelno, levelname",
    [
        (10, "DEBU"),
        (11, "LV11"),
        (20, "INFO"),
        (30, "WARN"),
        (40, "ERRO"),
        (50, "CRIT"),
    ],
)
def test_levelno_to_levelname_mapping(levelno, levelname, get_log):
    fw_logging.setup_logging(level="DEBUG")
    logging.log(levelno, "msg")
    assert get_log() == f"{TIMESTAMP} {levelname} {caller()} msg"


@pytest.mark.parametrize(
    "levelno, levelname, color",
    [
        (10, "DEBU", "cyan"),
        (11, "LV11", "green"),
        (20, "INFO", "green"),
        (30, "WARN", "yellow"),
        (40, "ERRO", "red"),
        (50, "CRIT", "magenta"),
    ],
)
def test_colors_with_tty(levelno, levelname, color, get_log, mocker):
    mocker.patch("sys.stdout.isatty", return_value=True)
    fw_logging.setup_logging(level="DEBUG")
    prefix = f"{TIMESTAMP} {colorize(color, levelname, bold=True)}"
    logging.log(levelno, "msg")
    assert get_log() == f"{prefix} {colorize('blue', caller())} msg"


def test_colors_disabled(get_log, mocker):
    mocker.patch("sys.stdout.isatty", return_value=True)
    fw_logging.setup_logging(color=False)
    logging.info("msg")
    assert get_log() == f"{TIMESTAMP} INFO {caller()} msg"


@pytest.mark.parametrize("level", ["debug", "info", "warning", "error", "critical"])
def test_root_logger_methods(level, get_log):
    levelname = level[:4].upper()
    fw_logging.setup_logging(level="DEBUG")
    logger_func = getattr(logging, level)
    logger_func("msg")
    assert get_log() == f"{TIMESTAMP} {levelname} {caller()} msg"


@pytest.mark.parametrize("level", ["debug", "info", "warning", "error", "critical"])
def test_custom_logger_methods(level, get_log):
    levelname = level[:4].upper()
    fw_logging.setup_logging(level="DEBUG")
    logger = logging.getLogger("test")
    getattr(logger, level)("msg")
    assert get_log() == f"{TIMESTAMP} {levelname} {caller(logger='test')} msg"


def test_file_handler(tmp_path):
    log_file = tmp_path / "log.txt"
    fw_logging.setup_logging(handler="file", filename=log_file)
    logging.info("msg")
    assert log_file.read_text() == f"{TIMESTAMP} INFO {caller()} msg\n"


def test_json_format(get_log):
    fw_logging.setup_logging(formatter="json")
    logging.info("msg")
    caller_info = caller()
    expected = (
        '{"message":"msg",'
        '"severity":"INFO",'
        f'"timestamp":"{TIMESTAMP}",'
        f'"caller":"{caller_info}",'
        r'"process":\d+,'
        r'"thread":\d+}'
    )
    assert re.match(expected, get_log())


def test_json_exc(get_log):
    fw_logging.setup_logging(formatter="json")
    logging.exception("msg")
    record = json.loads(get_log())
    assert record["exc"] == "NoneType: None"


def test_json_tag(get_log):
    fw_logging.setup_logging(formatter="json", json_tag="test")
    logging.info("msg")
    record = json.loads(get_log())
    assert record["tag"] == "test"


def test_filter_by_name(get_log):
    fw_logging.setup_logging()
    with fw_logging.Filter(name="test"):
        logging.getLogger("test").info("foo")
        logging.getLogger("root").info("bar")
    assert get_log() == f"{TIMESTAMP} INFO {caller()} bar"


def test_filter_by_msg(get_log):
    fw_logging.setup_logging()
    with fw_logging.Filter(msg="foo"):
        logging.getLogger("test").info("foo")
        logging.getLogger("root").info("bar")
    assert get_log() == f"{TIMESTAMP} INFO {caller()} bar"


TS = "1999-12-31T23:59:59.012456Z"
TB = 'Traceback (most recent call last):\\nFile \\"file\\", line 1, in mod\\n  call()'
MONGO = '{"command":{"aggregate":"subjects","durationMillis":453}}'
LOGS_IN = f"""
{{"message":"fw-logging","severity":"CRITICAL","caller":"test:1","timestamp":"{TS}"}}
{{"message":"next@2024-11-11T07:34:01.163","severity":"INFO","timestamp":"{TS}","caller":"xfer:1"}}
{{"message":"multi\\nline","severity":"ERROR","caller":"test:1","timestamp":"{TS}"}}
{{"message":"exc","severity":"WARNING","caller":"test:1","exc":"trace","timestamp":"{TS}"}}
{{"message":"flywheel-common","severity":"INFO","filename":"test.py","lineno":1,"timestamp":"{TS}"}}
{TS} DEBU prefix: {{"message":"exc_info","exc_info":"{TB}","t":"{TS}"}}
{{"msg":"engine","lvl":"dbug","action":"jobs-ask","t":"{TS}"}}
{{"msg":"engine","lvl":"eror","action":"jobs-ask","err":"err","t":"{TS}"}}
{{"time":"{TS}","remote_ip":"1.2.3.4","host":"host","method":"GET","uri":"/api/wamp","status":200,"bytes_out":0}}
{{"action":"heartbeat","lvl":"dbug","msg":"perim stats","stats":"...","t":"{TS}"}}
pod-name-hash pod-name stern
pod-name-hash pod-name {TS} stern --timestamp
{TS} kubectl logs --timestamps=true
unstructured text
{{"t":{{"$date":"{TS}"}},"s":"I","c":"COMMAND","msg":"Slow query","attr":{MONGO}}}
"""

LOGS_OUT = """
1999-12-31T23:59:59.012 CRIT test:1 fw-logging
1999-12-31T23:59:59.012 INFO xfer:1 next@2024-11-11T07:34:01.163
1999-12-31T23:59:59.012 ERRO test:1 multi
  line
1999-12-31T23:59:59.012 WARN test:1 exc
  trace
1999-12-31T23:59:59.012 INFO test.py:1 flywheel-common
1999-12-31T23:59:59.012 DEBU DEBU prefix: exc_info
  Traceback (most recent call last):
  File "file", line 1, in mod
    call()
1999-12-31T23:59:59.012 DEBU (jobs-ask) engine
1999-12-31T23:59:59.012 ERRO (jobs-ask) engine: err
1999-12-31T23:59:59.012      1.2.3.4 - "GET /api/wamp" 200
1999-12-31T23:59:59.012 DEBU (heartbeat) perim stats ...
                             pod-name-hash stern
1999-12-31T23:59:59.012      pod-name-hash stern --timestamp
1999-12-31T23:59:59.012      kubectl logs --timestamps=true
                             unstructured text
1999-12-31T23:59:59.012      Slow query
  {
    "command": {
      "aggregate": "subjects",
      "durationMillis": 453
    }
  }
""".lstrip()


def test_logformat(capsys, mocker):
    fileinput = mocker.patch("fw_logging.fileinput")
    fileinput.input.return_value = LOGS_IN.splitlines()
    fw_logging.logformat([])
    color_auto = capsys.readouterr()
    fw_logging.logformat(["--color", "never"])
    color_never = capsys.readouterr()
    assert color_auto.out == color_never.out == LOGS_OUT
    fw_logging.logformat(["--color", "always"])
    color_always = capsys.readouterr()
    assert strip_ansi(color_always.out) == LOGS_OUT


# TODO attempt to fix this test case
@pytest.mark.xfail(reason="logging implementation discrepancies across pyvers")
def test_add_log_level(get_log):
    fw_logging.add_log_level("ALERT", 60)
    fw_logging.setup_logging(level="DEBUG")

    # fails on 3.10:
    # got: '1999-12-31T23:59:59.012 ALER tests.test_fw_logging:267 foo'
    # exp: '1999-12-31T23:59:59.012 ALER root foo'
    assert getattr(logging, "ALERT") == 60
    logging.alert("foo")
    assert re.match(rf"{TIMESTAMP} ALER root foo", get_log())

    # fails on 3.13:
    # got: ''
    # exp: '1999-12-31T23:59:59.012 ALER root foo'
    logger = logging.getLogger("test")
    logger.alert("bar")
    assert re.match(rf"{TIMESTAMP} ALER test bar", get_log())
